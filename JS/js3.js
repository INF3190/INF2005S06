/**
 * Tableau associatif à éviter en Javascript -- 'node js3.js' pour tester 
 * Éviter d'utiliser new Array pour déclarer un tableau. C'est mieux []
 */
var tab1=[10,20,30,40];//tableau de 4 éléement
var tab2=[10];//tableau de 1 éléement
var tab3=new Array(10,20,30,40);//tableau de 4 éléments
var tab4=new Array(10);//tableau de 10 éléements
console.log(tab1.length);//4
console.log(tab2.length);//1
console.log(tab3.length);//4
console.log(tab4.length);//10
console.log(Array.isArray(tab3));//true tester si tableau
console.log(Array.isArray(tab4));//true
console.log(typeof tab3);//object
console.log(typeof tab4);//object
console.log(typeof tab3[1]);//nmber
console.log(typeof tab3[4]);//undefined
tab2[1]=12; 
console.log(tab2);// [ 10, 12 ]
console.log(tab2.length);//2
console.log(tab2[1]);//12
tab2["cle"]=7;
console.log(tab2);//[ 10, 12, cle: 7 ]
console.log(tab2.length);//2
console.log(tab2[3]);//undefined
console.log(tab2["cle"]);//7
var elem=tab2.pop();
console.log(elem);//12
console.log(tab2);//[ 10, cle: 7 ]
tab2.push(20);//ne place pas l'élément à la fin comme espéré
console.log(tab2);//[ 10, 20, cle: 7 ]