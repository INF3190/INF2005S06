/**
 * @author Johnny Tsheke
 */
'use strict';

console.log("var i of");
for (var i of [1,2,3,4,5,6,7,8,9,10]){
	console.log(i);//affiche valeur
}

console.log("var i in");
for (var i in [1,2,3,4,5,6,7,8,9,10]){
	console.log(i);//affiche clé
}