/**
 * @author Johnny Tsheke
 */
function addname() {
	var nom = document.getElementById("nomid").value;

	if (nom.length <= 1) {
		alert("trop court");
	} else {
		var indexe = localStorage.getItem("indexe");
		if ((indexe == null)||isNaN(indexe) || (indexe < 1)) {
			indexe = 1;
		}
		//enregistrement avec simulation de clé automatique
		localStorage.setItem(indexe, nom);
		//valeur de l'indexe comme clé
		indexe++;
		localStorage.setItem("indexe", indexe);
		//chaine "indexe" comme clé
		var contenu = "";
		for ( var i = 1; i < indexe; i++) {
			var elem = localStorage.getItem(i);
			//lire la valeur de la clé i
			contenu += "<li>" + elem + "</li>";
		}
		document.getElementById("liste").innerHTML = contenu;
	}
}
/*----suprime les donées enregistrées --*/
function deleteall(){
	var indexe = localStorage.getItem("indexe");
		if ((indexe == null) || (indexe < 1)) {
			indexe = 1;
		}
	  for( var i=1;i<indexe;i++){
	  	localStorage.setItem(i,null);//effacer
	  }// ou simplement localStorage.clear();
	  localStorage.setItem("indexe",0);
	  document.getElementById("liste").innerHTML="";
}
