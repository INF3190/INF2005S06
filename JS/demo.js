var tab = [10, 12, 4];
console.log(tab.length);
var tab1 = [10, 20, 30, 40];//tableau de 4 éléement
tab1.push(50);
tab1.push(40);
tab1["salut"] = "Bonjour";
var tab2=[10];//tableau de 1 éléement
var tab3=new Array(10,20,30,40);//tableau de 4 éléments
var tab4=new Array(10,12);//tableau de 10 éléements
console.log("tab1 = " + tab1.length);//4
console.log("tab1 complet " + tab1);
console.log("tab1 obj " + tab1.salut);
console.log(typeof tab1);
console.log(Array.isArray(tab1));
//console.log("dernier eleme tab1= " + tab1.pop(tab1));
console.log("tab1 apres pop = " + tab1.length);
console.log("indexof de 80 = " + tab1.lastIndexOf(80));
console.log("val avant derniere occ =" + tab1[tab1.lastIndexOf(40) - 1]);
console.log("tab2 = "+tab2.length);//1
console.log("tab3= "+tab3.length);//4
console.log("tab4= " + tab4.length);//10

for (elem in tab1) {
    //console.log("index= " + elem);
    console.log("valeur= " + tab1[elem]);
}